---
title: 本机 IP 地址查询工具
date: 2022-01-07
---

默认 API：https://stdout.com.cn/ip

## 参数

支持使用 query string 参数改变输出内容格

| key    | 可选值                  | 默认值   | 说明         |
|:-------|----------------------|-------|------------|
| lang   | de,en,fr,ja,ru,zh-CN | zh-CN | 输出内容语言     |
| format | text,json,xml,ml     | text  | 输出内容格式     |
| ip     | ipv4 only            | text  | 查询此IP地址的信息 |
| trace  |                      | false | 是否输出请求头信息  |

## format

text：默认输出格式，类似 myip.ipip.net

json：标准 json 格式

xml：标准 xml 格式

ml：多行文本

### 简写形式

https://stdout.com.cn/ip?$format

## 示例：

**以 xml 格式输出结果**

- https://stdout.com.cn/ip?format=xml
- https://stdout.com.cn/ip?xml

**以 json 格式输出对 8.8.8.8 的详细查询结果的简写形式**

https://stdout.com.cn/ip?json&trace&ip=8.8.8.8
