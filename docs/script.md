---
title: 实用 shell 脚本
date: 2022-01-07
---

## 查询脚本列表

```shell
curl -sL stdout.com.cn/@help | sh
```

## 调用方式

```shell 
curl -sL stdout.com.cn/@command | sh
```

## 命令列表预览

| Command     | 说明                       | 
|:------------|--------------------------|
| @cu.sh      | centos 换源                |
| @dt.sh      | 自动配置 debian apt 为腾讯 sid  |
| @ip.sh      | 打印当前IP                   |
| @kdump.sh   | 在 debian 上启用内核故障转储       |
| @n2n.sh     | 安装 n2n                   |
| @netinsp.sh | 网络出口IP检测                 |
| @os.sh      | 打印当前操作系统信息               |
| @sshdns.sh  | 关闭 ssh UseDNS            |
| @sshrf.sh   | 使ssh -R 可以监听公网           |
| @tz.sh      | 设置时区为上海                  |
| @ua.sh      | 更换 alpine 源为 ustc        |
| @ufpm.sh    | 在 ubuntu 中安装 fpm         |
| @um.sh      | 自动配置ubuntu 的 apt 源为 USTC |
| @wg.sh      | 安装和配置 Wireguard          |
| @zip.sh     | 安装 zip 工具                |
 
