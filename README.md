---
home: true
heroText: Alex
tagline: What's past is prologue.
# heroImage: https://stdout.com.cn/images/logo.png
# heroImageStyle: {
#    maxWidth: '100px',
#    width: '100%',
#    display: block,
#    margin: '5rem auto 3rem',
#    background: '#fff',
#    borderRadius: '1rem',
# }

bgImageStyle: {
    height: '450px'
}

isShowTitleInHome: false
actionText: Guide
actionLink: /views/other/guide
---
