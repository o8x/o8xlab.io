module.exports = {
    "title": "Alex",
    "description": "What's past is prologue.",
    "dest": "public",
    "head": [
        [
            "link",
            {
                "rel": "icon",
                "href": "/favicon.ico",
            },
        ],
        [
            "meta",
            {
                "name": "viewport",
                "content": "width=device-width,initial-scale=1,user-scalable=no",
            },
        ],
    ],
    "theme": "reco",
    "themeConfig": {
        "nav": [
            {
                "text": "Home",
                "link": "/",
                "icon": "reco-home",
            },
            {
                "text": "TimeLine",
                "link": "/timeline/",
                "icon": "reco-date",
            },
            {
                "text": "Docs",
                "icon": "reco-message",
                "link": "/docs/",
            },
            {
                "text": "About",
                "icon": "reco-account",
                "link": "/blogs/about.html",
            },
            {
                "text": "GitHub",
                "icon": "reco-github",
                "link": "https://github.com/o8x",
            },
        ],
        "subSidebar": "auto",
        "sidebar": {
            "/docs/": [
                "",
                "script",
            ],
        },
        "type": "blog",
        "blogConfig": {
            "tag": {
                "location": 3,
                "text": "Tags",
            },
        },
        "friendLink": [
            {
                "title": "一言",
                "desc": "一言指的就是一句话，可以是动漫中的台词，也可以是网络上的各种小段子。 或是感动，或是开心，有或是单纯的回忆。来到这里，留下你所喜欢的那一句句话，与大家分享，这就是一言存在的目的。",
                "email": "",
                "logo": "https://i.loli.net/2020/02/07/kDIUWCQpKEdFTP8.png",
                "link": "https://hitokoto.cn/",
            },
            {
                "title": "Ada",
                "desc": "",
                "email": "",
                "link": "https://www.adaxh.site/",
            },
            {
                "title": "羽子の部屋",
                "desc": "",
                "email": "",
                "logo": "https://reki.me/usr/themes/handsome/usr/img/sj2/8.jpg",
                "link": "https://reki.me/",
            },
            {
                "title": "腾之青",
                "desc": "亲眼所见，亦非真实",
                "email": "",
                "logo": "https://gravatar.loli.net/avatar/eccf8110b225c1ee784ba603e11213b5?d=mm&s=640",
                "link": "https://i.a632079.me/",
            },
            {
                "title": "站点聚合平台",
                "desc": "“因为热爱， 所以相聚”. 站点聚合平台，让更多的人记住您的网站.",
                "email": "",
                "link": "https://sites.applinzi.com",
            },
        ],
        "logo": "/logo.png",
        "search": true,
        "searchMaxSuggestions": 10,
        "lastUpdated": "Last Updated",
        "author": "Alex",
        "authorAvatar": "https://cdn-1252251443.cos.ap-nanjing.myqcloud.com/glogo.jpg",
        "record": "",
        "startYear": "2016",
    },
    "markdown": {
        "lineNumbers": true,
    },
}
