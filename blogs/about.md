---
title: Alex  
date: 2018-7-16 09:10:44
---

* rwrx@foxmail.com
* https://github.com/o8x

* 男 / 1996 / 3年 / 高级 Golang 工程师 / [一言开源社区成员](https://github.com/hitokoto-osc)

### 能力

- 基于 golang、C、python 的 B/S 与 C/S 应用开发
- 基于 2、3、4 层协议的应用开发
- web 应用、自动化运维软件、linux 内核模块、openwrt 模块、linux 防火墙软件开发
- 一定程度的运维与调优
- 较强的学习能力

### 技术栈

- Golang / Java / Linux / Python / React / Electron

### 其他

- 曾参与 [kataras/iris](https://github.com/kataras/iris) 框架贡献
- 曾参与 云霁(IDCOS) 开源项目 [CloudBoot](https://github.com/idcos/osinstall-server) 的贡献
- 曾参与 飞致云(FIT2CLOUD) 开源项目 [RackShift](https://github.com/rackshift/webkvm) 的贡献
- 开发过 Intellij Plugin https://github.com/hitokoto-osc/intellij-plugin
- 使用 Java 开发过 SDK https://github.com/hitokoto-osc/java-sdk
- 深入研究过 Drone https://segmentfault.com/a/1190000023161472
- 研究过 Kotlin 和爬虫 https://segmentfault.com/a/1190000012827066
- 深入使用过 Linux 系统 https://segmentfault.com/a/1190000015599137
