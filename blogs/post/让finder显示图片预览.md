---
title: 让finder显示图片预览
date: 2020-12-01 10:45:19
tags:

- MacOS

---



在finder中按下 ⌘+J 打开如下窗口，勾选显示图标预览

![image-20201201104529486](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/20201201104531.png)
