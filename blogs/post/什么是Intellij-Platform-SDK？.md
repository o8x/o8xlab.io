---
title: 什么是 Intellij Platform SDK ？
date: 2020-11-21 11:53:31
categories:
- Intellij Platform
tags:

- Intellij SDK

---

## IntelliJ Platform?

`Intellij Platform` 本身并不是一个产品，而是为构建 ide 提供了一个平台。它被用来驱动 JetBrains 产品，如 IntelliJ IDEA。它也是开源的，可以被第三方用来构建ide，比如 Google
的 [Android Studio](https://developer.android.com/studio/index.html)。

`Intellij Platform` 为这些IDE提供丰富语言工具，支持所需的所有基础设施。它是一个组件驱动的、基于JVM的跨平台应用程序，具有高级用户界面工具包，用于创建工具窗口、树视图和列表（支持快速搜索）以及弹出菜单和对话框。

## IntelliJ Platform SDK?

即用于为 `Intellij Platform`  开发扩展使用的软件开发工具包，提供了丰富的API和相关实例。

官方文档

- [https://jetbrains.org/intellij/sdk/docs/intro/welcome.html](https://jetbrains.org/intellij/sdk/docs/intro/welcome.html)
- [https://github.com/JetBrains/intellij-sdk-docs](https://github.com/JetBrains/intellij-sdk-docs)

官方示例：

- [https://github.com/JetBrains/intellij-sdk-code-samples](https://github.com/JetBrains/intellij-sdk-code-samples)

