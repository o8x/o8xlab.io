---
title: IDEA查看方法的详情
date: 2019-03-14 09:26:02
tags:  
- idea
---

我们有时候会按 ctrl + shift + - 然后再按 ctrl + + 收起方法体．只看到方法名和注释

效果就像这样

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/03/ef30874d681154a1012aedb2947a6855.png)

然后我们发现如果此时希望查看某个方法的内容就只能一级一级点开，或者把整个文件展开．
![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/03/2a5a9a940d3f9444e9578d9e9ec4ccfa.png)

当然这个不是我们想要的，于是快捷键 ctrl + shift + i 就出现了
光标放在方法名上，按 ctrl + shift + i
![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/03/d34f968a1497d71ba67604b1b64f617d.png)

这两个按钮，点左边是会编辑，右边是查看
![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/03/50c44abc7423a5575211132b049611a9.png)
