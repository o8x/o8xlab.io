---
title: 服务端解决跨域
date: 2018-02-03 20:43:13
tags:

- php
- nginx

---

### php

```shell
$ header('Access-Control-Allow-Origin: *');
  header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
  header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE')
```

#### nginx

```shell
$ add_header Access-Control-Allow-Origin "*";
  add_header Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept";
  add_header Access-Control-Allow-Methods "GET, POST, PUT,DELETE";
```

#### apache

```shell
$ Header set Access-Control-Allow-Origin "*"
  Header set Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Accept"
  Header set Access-Control-Allow-Methods "GET, POST, PUT,DELETE"
```
