---
title: "MacOS 桌面空间"
date: 2022-09-16T14:02:25+08:00
categories:
- MacOS
---

## MacOS 的 "分配给" 选项

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/clipboard_20220916_020957.png)

官方文档：https://support.apple.com/zh-cn/guide/mac-help/mh14112/mac

### 所有桌面

APP 会自动在当前显示器的当前桌面中打开，并且随着三指桌面切换手势应用也将会自动切换到当前桌面

### 显示器"1/2"的桌面

APP 将会自动在对应的显示器打开，即使你的主显示器或正在使用的显示器是另一个

### 无

APP 将会在当前显示器打开，并且不会随着三指切换桌面手势，切换应用所在的桌面
