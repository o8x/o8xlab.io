---
title: 强制改变windows的文本格式为unix格式
date: 2018-09-18 11:58:01
tags:  
- vim
---

> 很多时候windows上写的脚本上传到linux之后不能执行，都是因为 windows 标准的换行这一类特殊字符linux不能识别

> 强制把这些特殊字符转换到 unix 标准就好了

### 强制转换方法

- vim

```vim
:set ff=unix
```
