---
title: 让IDEA侧边栏的方法列表消失
date: 2018-07-12 20:58:38
tags:

- idea

---

# 让这个讨厌的东西消失

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2018/07/313f4c64331b1ecef4d925bd83790fd1.png)

# 就这样关闭show memebers，就不会出现了

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2018/07/4996643f7c27ddcb6bc365ee62d5f93d.png)

# 可以用 alt+7 让方法列表独立的展示出来

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2018/07/55f818d8fd5e372ceadd6d435acb194e.png)
