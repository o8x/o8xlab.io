---
title: 如何使用 gitmoji 将提交记录变的有趣
date: 2020-05-26 11:25:39
tags:

- git

---

**以下为`gitmoji --list` 结果集翻译**

🎨 `:art:`
> 改进代码的结构/格式

⚡️ `:zap:`
> 优化性能

🔥 `:fire:`
> 删除代码或文件

🐛 `:bug:`
> 修复bug

🚑 `:ambulance:`
> 关键性热布丁

✨ `:sparkles:`
> 加入新特性

📝 `:pencil:`
> 写入文档

🚀 `:rocket:`
> 部署的相关配置

💄 `:lipstick:`
> 修改样式或UI方面的内容

🎉 `:tada:`
> 里程碑，即项目的第一个commit

✅ `:white_check_mark:`
> 更新测试

🔒 `:lock:`
> 解决安全性问题

🍎 `:apple:`
> 修复在macos出现的bug

🐧 `:penguin:`
> 修复在linux上出现的问题

🏁 `:checkered_flag:`
> 修复在windows上出现的问题

🤖 `:robot:`
> 修复在android上出现问题

🍏 `:green_apple:`
> 修复在ios上出现的问题

🔖 `:bookmark:`
> 发布/标记版本

🚨 `:rotating_light:`
> 删除IDE的红线警告等

🚧 `:construction:`
> 工作的里程碑

💚 `:green_heart:`
> 修复CI构建问题

⬇️ `:arrow_down:`
> 移除一些依赖

⬆️ `:arrow_up:`
> 增加或更新一些依赖

📌 `:pushpin:`
> 将依赖关系固定到某一版本

👷 `:construction_worker:`
> 添加CI构建系统

📈 `:chart_with_upwards_trend:`
> 添加分析或跟踪代码

♻️ `:recycle:`
> 重构代码

🐳 `:whale:`
> 使代码可以工作在Docker中

➕ `:heavy_plus_sign:`
> 添加一个依赖项

➖ `:heavy_minus_sign:`
> 删除一个依赖

🔧 `:wrench:`
> 改变配置文件

🌐 `:globe_with_meridians:`
> 国际化和本地化

✏️ `:pencil2:`
> 修复拼写错误

💩 `:poop:`
> 编写需要改进的糟糕代码

⏪ `:rewind:`
> 撤销修改

🔀 `:twisted_rightwards_arrows:`
> 合并分支

📦 `:package:`
> 更新已编译的文件或包

👽 `:alien:`
> 由于外部API更改而更新代码

🚚 `:truck:`
> 移动或重命名文件

📄 `:page_facing_up:`
> 添加或更新许可证

💥 `:boom:`
> 引入不兼容的变更

🍱 `:bento:`
> 添加或更新资源文件

👌 `:ok_hand:`
> 由于代码评审更改而更新代码

♿️ `:wheelchair:`
> 提高可访问性

💡 `:bulb:`
> 记录源代码

🍻 `:beers:`
> 醉醺醺地编写代码

💬 `:speech_balloon:`
> 更新文案

🗃 `:card_file_box:`
> 执行与数据库相关的更改

🔊 `:loud_sound:`
> 添加日志

🔇 `:mute:`
> 删除日志

👥 `:busts_in_silhouette:`
> 添加贡献者

🚸 `:children_crossing:`
> 提高用户体验/可用性

🏗 `:building_construction:`
> *我不确定，知道的可以评论一下*

📱 `:iphone:`
> 增加或修改响应式设计

🤡 `:clown_face:`
> 加入Mock

🥚 `:egg:`
> 加入彩蛋

🙈 `:see_no_evil:`
> 添加或更新.gitignore 文件

📸 `:camera_flash:`
> 添加或更新快照

⚗ `:alembic:`
> 尝试新事物

🔍 `:mag:`
> 改善搜索引擎优化

☸️ `:wheel_of_dharma:`
> 使能工作在 Kubernetes 中

🏷️ `:label:`
> 添加或更新类型（检察）(Flow、TypeScript)

🌱 `:seedling:`
> 添加或更新数据库填充文件

🚩 `:triangular_flag_on_post:`
> 添加、更新或删除特性标志

🥅 `:goal_net:`
> 发现错误

💫 `:dizzy:`
> 添加或更新动画或转场特效

🗑 `:wastebasket:`
> 清理弃用代码
