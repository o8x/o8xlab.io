---
title: 关掉 idea 的参数名提示
date: 2018-11-01 14:44:44
tags:

- idea

---

关掉这堆让代码对不齐的灰色参数名提示

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2018/11/295bade20cd20f2b34be264e9a5f1133.png)

keymap里找到这个，设置快捷键即可使用

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2018/11/89774fa975dc39811fb1735051e67368.png)

# 大功告成

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2018/11/5e24ba7e1aa97bdcd5027f24f0e670f9.png)
