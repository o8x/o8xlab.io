---
title: 自定义 oh my zsh 主题
date: 2019-04-26 14:10:12
tags:

- Linux

---

> 默认主题全部位于 ~/.oh-my-zsh/themes/ , 本文基于自带主题 ys 修改

### 本期目标

去掉这个换行

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/04/c02d02ef178839007b8d7e9a57e921c1.png)

### 编辑 ys.zsh-theme

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/04/4646c5f8698ef7a32870d859daa67d73.png)

删掉 PROMPT 第一行自带的换行并保存重新登录 zsh

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/04/7254c905a66252cf4a8a4f9f74e022e0.png)

![](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/2019/04/d3939321c43a978095cdbed9c5f41efb.png)
