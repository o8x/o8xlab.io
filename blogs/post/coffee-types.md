---
title: 咖啡的分类  
date: 2023-04-24 14:07:20  
tags:

- 杂项

---

| 分类 | 详情                                                   |
|----|------------------------------------------------------|
|浓缩咖啡 | 烘焙好的咖啡豆磨碎，直接萃取得到的咖啡                                  |
|美式咖啡 | 浓缩咖啡 + 水                                             |
|拿铁 | 浓缩咖啡 + 奶 + 奶泡                                        |
|卡布奇诺 | 浓缩咖啡 + 奶 + 奶泡，但奶泡比拿铁较多                               |
|澳白/馥芮白 | 浓缩咖啡 + 奶，其中浓缩咖啡的量相较拿铁较多。 一般只有热饮，冷的馥芮白将会迅速的丧失风味，变成拿铁。 |
|摩卡 | 浓缩咖啡 + 奶 + 奶泡 + 巧克力酱                                 |
|焦糖玛奇朵 | 浓缩咖啡 + 奶 + 巧克力酱 + 焦糖糖浆 + 焦糖酱                         |
|康宝蓝 | 浓缩咖啡 + 奶油。较为少见                                       |
