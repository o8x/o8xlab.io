---
title: golang 工具链
date: 2020-07-16 08:53:33
categories:
- Golang
tags:

- golang

---

gofmt

格式化代码

godoc

- 在本地建立 golang 包文档官网

```shell
$ godoc -http=:8080 -play
using module mode; GOMOD=/private/tmp/goget/go.mod
```

![image-20200717153517805](https://alextech-1252251443.cos.ap-guangzhou.myqcloud.com/20200717153526.png)
